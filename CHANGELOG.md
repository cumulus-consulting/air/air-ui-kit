# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.4.1] - 2021-08-04
### Fixed
- Removed bottom border from last `tr` in a table

## [2.4.0] - 2021-07-28
### Added
- `.chip` style

## [2.3.0] - 2021-07-02
### Changed
- `.card` box-shadow
### Removed
- `.card` transition

## [2.2.3] - 2021-05-03
### Added
- Style for `dropdown-actions`

## [2.2.1] - 2021-05-03
### Added
- `.rotate-90` helper

## [2.2.0] - 2021-04-01
### Added
- `c-tabs` and `c-tab` classes
### Changed
- Deprecated `tabs` and `tab` classes, which will be removed in a future release. Use `c-tabs` and `c-tab` instead.

## [2.1.2] - 2021-03-08
### Fixed
- Replaced deprecated `overflow-y: overlay` with `overflow-y: auto` for `tbody` elements

## [2.1.1] - 2021-03-02
### Added
- `.cursor-pointer` helper class
### Changed
- Updated dropdown chevron to use Material icon
- `.color-red` helper is deprecated and will be removed in a future release. Use `.red` instead.

## [2.1.0] - 2021-03-01
### Added
- `.nvidia-green`, `.red`, and `.white` helper classes
### Changed
- Buttons now use `display: flex` for better alignment when embedding icons and text

### Changed
- Updated to match NVIDIA styles
### Added
- Standalone variables file for easy imports

## [1.4.1] - 2020-09-08
### Added
- Checkbox styles

## [1.4.0] - 2020-08-10
### Added
- `.cumulus-toast` styles

## [1.3.1] - 2020-07-02
### Added
- `.button-centered` helper

## [1.3.0] - 2020-06-29
### Added
- Baseline font size
- `a` style
- `.secondary-btn` style
- Style for disabled buttons
- Helper classes (.color-red, .jade-link, .text-centered)
### Changed
- Font defaults to Gotham

## [1.2.0] - 2020-06-11
### Added
- .tag style
### Changed
- Simplified button style

## [1.1.1] - 2020-04-09
### Added
- textarea style

## [1.1.0] - 2020-04-02
### Added
- .tabs and .tab styles
### Changed
- .label style is now small, bold, and uppercase
- Updated box shadow and border colors to be consistent across all inputs

## [1.0.3] - 2020-03-23
### Added
- .card-footer style

## [1.0.2] - 2020-03-20
### Added
- .card style

## [1.0.1] - 2020-03-19
### Added
- input: Added style
- button: Added style
- Classes: Added .label

## [1.0.0] - 2020-03-04
### Added
Initial release
