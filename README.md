# air-ui-kit

The AIR UI kit provides a CSS-only UI framework for front end web development. It aims to conform to the same styles used by https://www.nvidia.com.

## Usage

### Install the library
```
npm install git+ssh://gitlab.com:cumulus-consulting/air/air-ui-kit.git
```

### Import the library into your project
```
@import "~@cumulus/air-ui-kit";
```
